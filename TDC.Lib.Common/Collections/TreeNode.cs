﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace TDC.Lib.Common.Collections
{
    /// <summary>
    /// Source: https://github.com/gt4dev/yet-another-tree-structure
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class TreeNode<T> : IEnumerable<TreeNode<T>>
    {
        public TreeNode(T data)
        {
            Data = data;
            Children = new LinkedList<TreeNode<T>>();

            ElementsIndex = new LinkedList<TreeNode<T>>();
            ElementsIndex.Add(this);
        }

        public ICollection<TreeNode<T>> Children { get; set; }

        public T Data { get; set; }

        public bool IsLeaf => Children.Count == 0;

        public bool IsRoot => Parent == null;

        public int Level
        {
            get
            {
                if (IsRoot)
                    return 0;
                return Parent.Level + 1;
            }
        }

        public TreeNode<T> Parent { get; set; }

        public TreeNode<T> AddChild(T child)
        {
            var childNode = new TreeNode<T>(child) {Parent = this};
            Children.Add(childNode);

            RegisterChildForSearch(childNode);

            return childNode;
        }

        public IList<TreeNode<T>> AddChildren(IList<T> children)
        {
            var childrenNodes = new List<TreeNode<T>>();
            foreach (var child in children)
            {
                childrenNodes.Add(AddChild(child));
            }
            return childrenNodes;
        }

        public override string ToString()
        {
            return Data != null ? Data.ToString() : "[data null]";
        }


        #region searching

        private ICollection<TreeNode<T>> ElementsIndex { get; }

        private void RegisterChildForSearch(TreeNode<T> node)
        {
            ElementsIndex.Add(node);
            if (Parent != null)
                Parent.RegisterChildForSearch(node);
        }

        public TreeNode<T> FindTreeNode(Func<TreeNode<T>, bool> predicate)
        {
            return ElementsIndex.FirstOrDefault(predicate);
        }

        public LinkedList<TreeNode<T>> GetPathToRoot()
        {
            if(IsRoot) return new LinkedList<TreeNode<T>>(this);

            var retVal = new LinkedList<TreeNode<T>>();

            var currentNode = this;
            while (!currentNode.IsRoot)
            {
                retVal.AddLast(currentNode);
                currentNode = currentNode.Parent;
            }
            retVal.AddLast(currentNode);
            return retVal;
        }

        public ICollection<TreeNode<T>> GetLeaves(ICollection<TreeNode<T>> leafes = null)
        {
            if (IsLeaf) return new List<TreeNode<T>> { this };

            if (leafes == null)
                leafes = new List<TreeNode<T>>();

            foreach (var subTree in Children)
            {
                if (subTree.IsLeaf)
                    leafes.Add(subTree);
                else GetLeaves(subTree, leafes);
            }

            return leafes;
        }

        public void RemoveDuplicates()
        {
            if (Children == null || !Children.Any()) return;

            var childrenToHold = new List<TreeNode<T>>();

            foreach (var child in Children)
            {
                if (childrenToHold.Any(t => t.Data.Equals(child.Data))) continue;
                childrenToHold.Add(child);
            }

            Children.Clear();
            
            foreach (var child in childrenToHold)
            {
                Children.Add(child);
            }
        }

        #region statics
        public static ICollection<TreeNode<T>> GetLeaves(TreeNode<T> root, ICollection<TreeNode<T>> leafes = null)
        {
            if (root == null) return null;
            if (root.IsLeaf) return new List<TreeNode<T>> { root };

            if (leafes == null)
                leafes = new List<TreeNode<T>>();

            foreach (var subTree in root.Children)
            {
                if (subTree.IsLeaf)
                    leafes.Add(subTree);
                else GetLeaves(subTree, leafes);
            }

            return leafes;
        }

        public static ICollection<TreeNode<T>> RemoveDuplicates(ICollection<TreeNode<T>> source)
        {
            if (source == null) return null;
            var retVal = new List<TreeNode<T>>();

            foreach (var treeNode in source)
            {
                if (retVal.Any(t => t.Data.Equals(treeNode.Data))) continue;
                retVal.Add(treeNode);
            }
            return retVal;
        }
        #endregion

        #endregion


        #region iterating

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IEnumerator<TreeNode<T>> GetEnumerator()
        {
            yield return this;
            foreach (var directChild in Children)
            foreach (var anyChild in directChild)
                yield return anyChild;
        }

        #endregion
    }
}