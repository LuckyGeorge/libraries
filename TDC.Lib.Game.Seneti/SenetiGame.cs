﻿using System;
using System.Collections.Generic;
using System.Linq;
using TDC.Lib.Common.Collections;
using TDC.Lib.Game.Seneti.Player;
using TDC.Lib.Game.Seneti.Tiles;
using TDC.Lib.Game.Seneti.Token;

namespace TDC.Lib.Game.Seneti
{
    public class SenetiGame
    {
        private List<IPlayer> _players;
        private List<ITile> _board;
        private List<IToken> _tokens;

        #region Events

        public event EventHandler OnInitialized;

        #endregion

        public SenetiGame()
        {

        }

        public IPlayer CurrentPlayer { get; set; }

        public List<ITile> Board => _board;

        public int Rows
        {
            get
            {
                if (_board == null) return 0;
                var minY = _board.Min(x => x.Position.Y);
                var maxY = _board.Max(x => x.Position.Y);

                if (minY.HasValue && maxY.HasValue) return (maxY.Value - minY.Value) +1;
                return 0;
            }
        }

        public int Columns
        {
            get
            {
                if (_board == null) return 0;
                var minX = _board.Min(x => x.Position.X);
                var maxX = _board.Max(x => x.Position.X);

                if (minX.HasValue && maxX.HasValue) return (maxX.Value - minX.Value) +1;
                return 0;
            }
        }

        public void Initialize(IPlayer playerOne, IPlayer playerTwo)
        {
            if (playerOne == null) throw new ArgumentNullException(nameof(playerOne));
            if (playerTwo == null) throw new ArgumentNullException(nameof(playerTwo));
            if (playerOne.Color == playerTwo.Color) throw new ArgumentException("Players must have different colors!");

            //Create Players
            if (_players != null)
            {
                _players.Clear();
                _players.Add(playerOne);
                _players.Add(playerTwo);
            }
            else
                _players = new List<IPlayer> { playerOne, playerTwo };

            //Create Tokens and add them to players
            if(_tokens != null) _tokens.Clear();
            else _tokens = new List<IToken>();
            for (int i = 0; i < 14; i += 2)
            {
                _tokens.Add(new Token.Token(playerOne));
                _tokens.Add(new Token.Token(playerTwo));
            }

            //Create Board
            _board = new List<ITile>
            {
                //Top Line
                new Tile(new Position(0, 0), TileType.Rosetta),
                new Tile(new Position(1, 0), TileType.ThreeAndEntry),
                new Tile(new Position(2, 0), TileType.One),
                new Tile(new Position(3, 0), TileType.ThreeAndEntry),
                new Tile(new Position(4, 0), TileType.None),
                new Tile(new Position(5, 0), TileType.None),
                new Tile(new Position(6, 0), TileType.Rosetta),
                new Tile(new Position(7, 0), TileType.Two),
                //Center Line
                new Tile(new Position(0, 1), TileType.Six),
                new Tile(new Position(1, 1), TileType.One),
                new Tile(new Position(2, 1), TileType.Four),
                new Tile(new Position(3, 1), TileType.Rosetta),
                new Tile(new Position(4, 1), TileType.Four),
                new Tile(new Position(5, 1), TileType.One),
                new Tile(new Position(6, 1), TileType.ThreeAndEntry),
                new Tile(new Position(7, 1), TileType.One),
                //Bottom Line
                new Tile(new Position(0, 2), TileType.Rosetta),
                new Tile(new Position(1, 2), TileType.ThreeAndEntry),
                new Tile(new Position(2, 2), TileType.One),
                new Tile(new Position(3, 2), TileType.ThreeAndEntry),
                new Tile(new Position(4, 2), TileType.None),
                new Tile(new Position(5, 2), TileType.None),
                new Tile(new Position(6, 2), TileType.Rosetta),
                new Tile(new Position(7, 2), TileType.Two),
            };

            foreach (var tile in Board)
            {
                tile.SetBoard(Board);
            }

            OnInitialized?.Invoke(this,null);
        }

        public IDictionary<ITile, TreeNode<ITile>> GetEntryTargetsAsTree()
        {

            var retVal = new Dictionary<ITile, TreeNode<ITile>>();
            if (_board != null)
            {
                var entryTiles = _board.Where(t => t.IsEntry);
                foreach (var entryTile in entryTiles)
                {
                    var targetTiles = GetNextTargetsAsTree(entryTile, 4);
                    if (targetTiles.Any())
                        retVal.Add(entryTile, targetTiles);
                }
            }
            return retVal;
        }

        public TreeNode<ITile> GetNextTargetsAsTree(ITile source, int distance, TreeNode<ITile> currentTree = null, int currentStep = 0)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            if (distance < 0 || distance > 6) throw new ArgumentOutOfRangeException(nameof(distance));
            if (currentTree == null)
            {
                currentTree = new TreeNode<ITile>(source);
                currentTree.AddChildren(distance == 1 ? source.GetNeighbours(TileBlockType.BlockedForPlacingToken) : source.GetNeighbours(TileBlockType.BlockedForPassingOver));
                currentStep++;
            }

            if (currentStep == distance)
            {
                var invalidLeaves = currentTree.GetLeaves().Where(leaf => leaf.Level != distance);
                foreach (var leaf in invalidLeaves)
                {
                    
                }
                return currentTree;
            }

            var step = currentStep;
            var leaves = TreeNode<ITile>.GetLeaves(currentTree);

            foreach (var leaf in leaves)
            {
                var neighbours = currentStep == distance - 1
                    ? leaf.Data.GetNeighbours(TileBlockType.BlockedForPlacingToken)
                    : leaf.Data.GetNeighbours(TileBlockType.BlockedForPassingOver);

                var validNeighbours = leaf.Parent != null ? neighbours.Where(n => !n.IsEqualTo(leaf.Parent.Data)).ToList() : neighbours;

                leaf.AddChildren(validNeighbours);
            }
            currentStep++;

            return GetNextTargetsAsTree(source, distance, currentTree, currentStep);

        }

        public LinkedList<TreeNode<ITile>> GetPath(ITile source, ITile requestedTarget)
        {
            if (source == null || requestedTarget == null || Equals(source, requestedTarget)) return null;

            var possiblePathsFromSource = GetNextTargetsAsTree(source, (int) source.TileType);
            var possibleTargets = possiblePathsFromSource.GetLeaves();

            var target = possibleTargets.FirstOrDefault(possibleTarget => possibleTarget.Data.IsEqualTo(requestedTarget));

            return target?.GetPathToRoot();
        }

        public string PrintBoard()
        {
            if (Board != null && Board.Count > 0)
            {
                var boardString = "---------------------------------" + Environment.NewLine;
                for (int y = 0; y < 3; y++)
                {
                    var rowString = "|";
                    for (int x = 0; x < 8; x++)
                    {
                        var tile = Board.FirstOrDefault(t => t.Position.IsEqualTo(new Position((byte) x, (byte) y)));
                        if (tile != null)
                            rowString += $" {(int) tile.TileType} |";
                    }
                    boardString += rowString + Environment.NewLine;
                }
                boardString += "---------------------------------";
                return boardString;
            }
            return string.Empty;
        }

        public string PrintTileList()
        {
            if (Board != null && Board.Count > 0)
            {
                var tileListString = string.Empty;
                foreach (var tile in Board)
                {
                    tileListString += $"{(int)tile.TileType}\t{tile.Position}";
                    foreach (var neighbour in tile.GetNeighbours(TileBlockType.NotBlocked))
                    {
                        tileListString += $"\t{(int)neighbour.TileType}";
                    }
                    tileListString += Environment.NewLine;
                }
                return tileListString;
            }
            return string.Empty;
        }
    }
}
