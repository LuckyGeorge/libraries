﻿using System.Collections.Generic;
using TDC.Lib.Game.Seneti.Token;

namespace TDC.Lib.Game.Seneti.Player
{
    public class Human : IPlayer
    {
        public Human(string name, PlayerColor color)
        {
            Name = name;
            Color = color;
        }
        public string Name { get; }
        public PlayerColor Color { get; }
        public List<IToken> Tokens { get; } = new List<IToken>();
    }
}