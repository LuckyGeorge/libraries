﻿using System.Collections.Generic;
using TDC.Lib.Game.Seneti.Token;

namespace TDC.Lib.Game.Seneti.Player
{
    public interface IPlayer
    {
        string Name { get; }
        PlayerColor Color { get; }
        List<IToken> Tokens { get; }
    }
}