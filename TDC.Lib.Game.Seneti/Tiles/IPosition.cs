﻿namespace TDC.Lib.Game.Seneti.Tiles
{
    public interface IPosition
    {
        byte? X { get; }
        byte? Y { get; }
        bool IsEqualTo(IPosition position);
    }
}