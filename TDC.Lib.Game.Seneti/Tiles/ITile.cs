﻿using System.Collections.Generic;
using TDC.Lib.Game.Seneti.Token;

namespace TDC.Lib.Game.Seneti.Tiles
{
    public interface ITile
    {
        IPosition Position { get; }
        TileType TileType { get; }
        Stack<IToken> Tokens { get; }
        bool IsEntry { get; }
        TileBlockType BlockType { get; }
        bool IsBlocked { get; }
        bool IsBlockedByToken { get; }
        bool IsBlockedByTileType { get; }
        bool IsBlockedByNumberOfTokens { get; }
        IList<ITile> GetNeighbours(TileBlockType block = TileBlockType.BlockedForPlacingToken);
        void SetBoard(IList<ITile> board);
        bool IsEqualTo(ITile tile);
    }
}