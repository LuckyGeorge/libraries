﻿using System;

namespace TDC.Lib.Game.Seneti.Tiles
{
    public class Position : IPosition, IEquatable<IPosition>
    {
        public Position(byte x, byte y)
        {
            X = x;
            Y = y;
        }

        public byte? X { get; }
        public byte? Y { get; }

        public override bool Equals(object obj)
        {
            if (null == obj) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Position) obj);
        }

        public bool IsEqualTo(IPosition position)
        {
            if (position == null) return false;
            return position.X == X && position.Y == Y;
        }


        public bool Equals(IPosition other)
        {
            if (other == null) return false;
            return X == other.X && Y == other.Y;
        }

        public override string ToString()
        {
            return $"({X}/{Y})";
        }

        protected bool Equals(Position other)
        {
            return X == other.X && Y == other.Y;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (X.GetHashCode() * 397) ^ Y.GetHashCode();
            }
        }
    }
}