﻿namespace TDC.Lib.Game.Seneti.Tiles
{
    public enum TileBlockType
    {
        NotBlocked,
        BlockedForPassingOver,
        BlockedForPlacingToken,
    }
}