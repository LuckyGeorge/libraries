﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using TDC.Lib.Game.Seneti.Token;

namespace TDC.Lib.Game.Seneti.Tiles
{
    public class Tile : ITile, IEquatable<ITile>
    {
        private IList<ITile> _board;
        public Tile(IPosition position, TileType type)
        {
            Position = position;
            TileType = type;
        }

        public void SetBoard(IList<ITile> board)
        {
            _board = board ?? throw new ArgumentNullException(nameof(board));
        }

        public IList<ITile> GetNeighbours(TileBlockType block = TileBlockType.BlockedForPlacingToken)
        {
            var neighbours = new List<ITile>();

            if (_board != null)
            {
                neighbours.AddRange(_board.Where(tile => tile != this && tile.TileType != TileType.None && tile.Position != null)
                    .Where(tile => tile.Position.X == Position.X &&
                                   (tile.Position.Y == Position.Y + 1 || tile.Position.Y == Position.Y - 1) ||
                                   tile.Position.Y == Position.Y &&
                                   (tile.Position.X == Position.X + 1 || tile.Position.X == Position.X - 1))
                    .Where(tile => tile.BlockType != block));
            }
            return neighbours;
        }

        public IPosition Position { get; }
        public TileType TileType { get; }
        public Stack<IToken> Tokens { get; } = new Stack<IToken>();
        public bool IsEntry => TileType == TileType.ThreeAndEntry;

        public TileBlockType BlockType
        {
            get
            {
                if(IsBlocked) return TileBlockType.BlockedForPlacingToken;
                if(IsBlockedByToken || TileType == TileType.None) return TileBlockType.BlockedForPassingOver;
                return TileBlockType.NotBlocked;
            }
        }

        public bool IsBlocked => IsBlockedByToken || IsBlockedByNumberOfTokens || IsBlockedByTileType;
        public bool IsBlockedByToken => Tokens.Count > 0 && !Tokens.Peek().IsOpen;
        public bool IsBlockedByTileType => TileType == TileType.Rosetta || TileType == TileType.None;
        public bool IsBlockedByNumberOfTokens => Tokens.Count >= (int) TileType && IsBlockedByToken;

        public bool IsEqualTo(ITile tile)
        {
            if (tile == null) return false;
            return Position.IsEqualTo(tile.Position) && TileType == tile.TileType;
        }

        public bool Equals(ITile other)
        {
            if (other == null) return false;
            return Equals(Position, other.Position) && TileType == other.TileType;
        }

        public override string ToString()
        {
            return $"{(int)TileType} - {Position}";
        }

        protected bool Equals(Tile other)
        {
            return Equals(Position, other.Position) && TileType == other.TileType;
        }

        public override bool Equals(object obj)
        {
            if (null == obj) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Tile) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((Position != null ? Position.GetHashCode() : 0) * 397) ^ (int) TileType;
            }
        }
    }
}