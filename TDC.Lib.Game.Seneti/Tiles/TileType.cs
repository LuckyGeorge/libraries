﻿namespace TDC.Lib.Game.Seneti.Tiles
{
    public enum TileType
    {
        None = 0,
        One = 1,
        Two = 2,
        ThreeAndEntry = 3,
        Four = 4,
        Six = 6,
        Rosetta = 7,
    }
}