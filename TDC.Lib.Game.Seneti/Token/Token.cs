﻿using System;
using System.Collections.Generic;
using System.Linq;
using TDC.Lib.Game.Seneti.Player;
using TDC.Lib.Game.Seneti.Tiles;

namespace TDC.Lib.Game.Seneti.Token
{
    public class Token : IToken
    {
        public Token(IPlayer player)
        {
            Player = player;
            Tile = null;
            IsOpen = true;
            Player.Tokens.Add(this);
        }

        public IPlayer Player { get; }
        public ITile Tile { get; private set; }
        public PlayerColor Color => Player.Color;
        public bool IsOpen { get; private set; }
        public bool MoveTo(ITile target)
        {
            if (target != null && target != Tile)
            {
                Tile = target;
                IsOpen = !IsOpen;
                return true;
            }
            return false;
        }

        public bool Exit()
        {
            if (Tile != null)
            {
                Tile = null;
                IsOpen = false;
                return true;
            }
            return false;
        }

        public bool IsValidTarget(ITile target)
        {
            if (target == null && Tile != null) return true; //Exit

            if (target != null)
            {
                if (Tile == null && target.IsEntry) return true; //Enter

                if (target.IsBlockedByTileType) return false; // Block

                if (target.IsBlockedByToken) return false; //Block by token

                if (target.IsBlockedByNumberOfTokens) return false; //to many tokens on target
                
                return true;
            }
            return false;
        }

        public List<List<ITile>> GetPathsFrom()
        {
            var paths = new List<List<ITile>>();

            return paths;
        }

        public override string ToString()
        {
            return $"{Player.Name} + {Player.Color} + {Tile}";
        }
    }
}