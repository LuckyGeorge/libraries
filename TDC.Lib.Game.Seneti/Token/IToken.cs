﻿using System.Collections.Generic;
using TDC.Lib.Game.Seneti.Player;
using TDC.Lib.Game.Seneti.Tiles;

namespace TDC.Lib.Game.Seneti.Token
{
    public interface IToken
    {
        IPlayer Player { get; }
        ITile Tile { get; }
        PlayerColor Color { get; }
        bool IsOpen { get; }
        bool MoveTo(ITile target);
        bool Exit();
        bool IsValidTarget(ITile target);
        List<List<ITile>> GetPathsFrom();
    }
}