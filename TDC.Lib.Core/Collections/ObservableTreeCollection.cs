﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;

namespace TDC.Lib.Core.Collections
{
    public interface ITree
    {
        bool HasItems { get; }
        void Add(object data);
    }

    public interface ITree<T> : ITree, IEnumerable<ITree<T>>
    {
        T Data { get; }
        IList<ITree<T>> Items { get; }
        ITree<T> Parent { get; }
        IEnumerable<ITree<T>> GetChildren(bool includingThis);
        IEnumerable<ITree<T>> GetParents(bool includingThis);
    }

    public class ObservableTreeCollection<T> : ITree<T>
    {
        private ObservableCollection<ITree<T>> _items;

        public ObservableTreeCollection(T data)
            : this(data, null)
        {
        }

        public ObservableTreeCollection(T data, params object[] items)
        {
            Data = data;
            Add(items);
        }

        public ObservableTreeCollection(T data, params ITree<T>[] items)
        {
            Data = data;
            Add(items);
        }

        public void Add(object data)
        {
            if (data == null)
                return;

            if (data is T)
            {
                Items.Add(CloneNode(new ObservableTreeCollection<T>((T) data)));
                return;
            }

            if (data is ITree<T> t)
            {
                Items.Add(CloneTree(t));
                return;
            }

            if (data is object[] o)
            {
                foreach (var obj in o)
                    Add(obj);
                return;
            }

            if (data is IEnumerable e && !(data is ITree))
            {
                foreach (var obj in e)
                    Add(obj);
                return;
            }

            throw new InvalidOperationException("Cannot add unknown content type.");
        }

        public T Data { get; }

        public bool HasItems => _items != null && _items.Count > 0;

        public ITree<T> Parent { get; private set; }

        public IList<ITree<T>> Items
        {
            get
            {
                if (_items == null)
                {
                    _items = new ObservableCollection<ITree<T>>();
                    _items.CollectionChanged += ItemsOnCollectionChanged;
                }
                return _items;
            }
        }

        public IEnumerable<ITree<T>> GetParents(bool includingThis)
        {
            if (includingThis)
                yield return this;

            var parent = Parent;
            while (parent != null)
            {
                yield return parent;
                parent = parent.Parent;
            }
        }

        public IEnumerable<ITree<T>> GetChildren(bool includingThis)
        {
            if (includingThis)
                yield return this;

            if (_items != null)
                foreach (var child in _items.SelectMany(item => item.GetChildren(true)))
                    yield return child;
        }

        public IEnumerator<ITree<T>> GetEnumerator()
        {
            return _items?.GetEnumerator() ?? Enumerable.Empty<ITree<T>>().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        protected virtual ITree<T> CloneNode(ITree<T> item)
        {
            return new ObservableTreeCollection<T>(item.Data);
        }

        public ITree<T> CloneTree(ITree<T> item)
        {
            var result = CloneNode(item);
            if (item.HasItems)
                result.Add(item.Items);
            return result;
        }

        private void ItemsOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs args)
        {
            if (args.Action == NotifyCollectionChangedAction.Add && args.NewItems != null)
                foreach (var item in args.NewItems.Cast<ObservableTreeCollection<T>>())
                    item.Parent = this;
            else if (args.Action != NotifyCollectionChangedAction.Move && args.OldItems != null)
                foreach (var item in args.OldItems.Cast<ObservableTreeCollection<T>>())
                {
                    item.Parent = null;
                    item.ResetOnCollectionChangedEvent();
                }
        }

        private void ResetOnCollectionChangedEvent()
        {
            if (_items != null)
                _items.CollectionChanged -= ItemsOnCollectionChanged;
        }

        public override string ToString()
        {
            if (typeof(T).IsValueType)
                return Data.ToString();
            return EqualityComparer<T>.Default.Equals(Data, default(T)) ? string.Empty : Data.ToString();
        }
    }

    public static class ObservableTreeCollectionFactory
    {
        public static ITree<T> Create<T>(T data)
        {
            return new ObservableTreeCollection<T>(data);
        }

        public static ITree<T> Create<T>(T data, params ITree<T>[] items)
        {
            return new ObservableTreeCollection<T>(data, items);
        }

        public static ITree<T> Create<T>(T data, params object[] items)
        {
            return new ObservableTreeCollection<T>(data, items);
        }

        public static void Visit<T>(ITree<T> tree, Action<ITree<T>> action)
        {
            action(tree);
            if (tree.HasItems)
                foreach (var item in tree)
                    Visit(item, action);
        }

        public static IList<T> GetLeaves<T>(ITree<T> tree, IList<T> leaves = null)
        {
            if (tree == null || !tree.HasItems) return null;

            if(leaves == null)
                leaves = new List<T>();

            foreach (var subTree in tree)
            {
                if (!subTree.HasItems)
                    leaves.Add(subTree.Data);
                else GetLeaves(subTree, leaves);
            }

            return leaves;
        }
    }
}