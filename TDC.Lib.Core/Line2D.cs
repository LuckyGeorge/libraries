﻿using System.Windows;
using ControlzEx.Standard;

namespace TDC.Lib.Core
{
    public class Line2D
    {
        public double X1 => P1.X;
        public double X2 => P2.X;
        public double Y1 => P1.Y;
        public double Y2 => P2.Y;

        public Point P1 { get; }

        public Point P2 { get; }

        public Line2D(double x1, double y1, double x2, double y2)
        {
            P1 = new Point(x1, y1);
            P2 = new Point(x2, y2);
        }

        public Line2D(Point p1, Point p2)
        {
            P1 = new Point(p1.X, p1.Y);
            P2 = new Point(p2.X, p2.Y);
        }

        public override string ToString()
        {
            return$"({X1}:{Y1}) - ({X2}:{Y2})";
        }
    }
}