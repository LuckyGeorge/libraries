﻿using System;

namespace TDC.Lib.Core.Tools
{
    public sealed class Singleton<T> where T : class, new()
    {
        private static readonly Lazy<T> instance = new Lazy<T>(() => new T());

        public static T Instance => instance.Value;

        private Singleton() { }
    }
}