﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace TDC.Lib.Core.WPF.AttachedProperties
{
    public class AttachedGridProperties
    {
        #region RowCount Property

        /// <summary>
        /// Adds the specified number of Rows to RowDefinitions. 
        /// Default Height is <see cref="GridUnitType.Star"/>
        /// </summary>
        public static readonly DependencyProperty RowCountProperty =
            DependencyProperty.RegisterAttached(
                "RowCount", typeof(int), typeof(AttachedGridProperties),
                new PropertyMetadata(-1, RowCountChanged));

        // Get
        public static int GetRowCount(DependencyObject obj)
        {
            return (int)obj.GetValue(RowCountProperty);
        }

        // Set
        public static void SetRowCount(DependencyObject obj, int value)
        {
            obj.SetValue(RowCountProperty, value);
        }

        // Change Event - Adds the Rows
        public static void RowCountChanged(
            DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if (!(obj is Grid) || (int)e.NewValue < 0)
                return;

            Grid grid = (Grid)obj;
            grid.RowDefinitions.Clear();


            for (var i = 0; i < (int) e.NewValue; i++)
                grid.RowDefinitions.Add(
                    new RowDefinition
                    {
                        Height = GetRowHeight(obj)
                    });
        }

        #endregion

        #region RowHeight Property

        /// <summary>
        /// Sets the Height of all Rows
        /// </summary>
        public static readonly DependencyProperty RowHeightProperty =
            DependencyProperty.RegisterAttached(
                "RowHeight", typeof(GridLength), typeof(AttachedGridProperties),
                new PropertyMetadata(GridLength.Auto, RowHeightChanged));

        // Get
        public static GridLength GetRowHeight(DependencyObject obj)
        {
            return (GridLength)obj.GetValue(RowHeightProperty);
        }

        // Set
        public static void SetRowHeight(DependencyObject obj, GridLength value)
        {
            obj.SetValue(RowHeightProperty, value);
        }

        // Change Event - Adds the Rows
        public static void RowHeightChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if (!(obj is Grid) || !(e.NewValue is GridLength))
                return;

            Grid grid = (Grid)obj;

            foreach (var rowDefinition in grid.RowDefinitions)
            {
                rowDefinition.Height = (GridLength)e.NewValue;
            }
        }

        #endregion

        #region ColumnCount Property

        /// <summary>
        /// Adds the specified number of Columns to ColumnDefinitions. 
        /// Default Width is <see cref="GridUnitType.Star"/>
        /// </summary>
        public static readonly DependencyProperty ColumnCountProperty =
            DependencyProperty.RegisterAttached(
                "ColumnCount", typeof(int), typeof(AttachedGridProperties),
                new PropertyMetadata(-1, ColumnCountChanged));

        // Get
        public static int GetColumnCount(DependencyObject obj)
        {
            return (int)obj.GetValue(ColumnCountProperty);
        }

        // Set
        public static void SetColumnCount(DependencyObject obj, int value)
        {
            obj.SetValue(ColumnCountProperty, value);
        }

        // Change Event - Add the Columns
        public static void ColumnCountChanged(
            DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if (!(obj is Grid) || (int)e.NewValue < 0)
                return;

            Grid grid = (Grid)obj;
            grid.ColumnDefinitions.Clear();

            for (int i = 0; i < (int)e.NewValue; i++)
                grid.ColumnDefinitions.Add(
                    new ColumnDefinition()
                    {
                        Width = GetColumnWidth(obj)
                    });
        }

        #endregion

        #region ColumnWidth Property

        /// <summary>
        /// Sets the Width of all Columns
        /// </summary>
        public static readonly DependencyProperty ColumnWidthProperty =
            DependencyProperty.RegisterAttached(
                "ColumnWidth", typeof(GridLength), typeof(AttachedGridProperties),
                new PropertyMetadata(GridLength.Auto, ColumnWidthChanged));

        // Get
        public static GridLength GetColumnWidth(DependencyObject obj)
        {
            return (GridLength)obj.GetValue(ColumnWidthProperty);
        }

        // Set
        public static void SetColumnWidth(DependencyObject obj, GridLength value)
        {
            obj.SetValue(ColumnWidthProperty, value);
        }

        // Change Event - Adds the Rows
        public static void ColumnWidthChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if (!(obj is Grid) || !(e.NewValue is GridLength))
                return;

            Grid grid = (Grid)obj;

            foreach (var columnDefinition in grid.ColumnDefinitions)
            {
                columnDefinition.Width = (GridLength)e.NewValue;
            }
        }

        #endregion
    }
  
}