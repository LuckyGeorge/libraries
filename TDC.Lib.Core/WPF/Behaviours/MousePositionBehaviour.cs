﻿using System.Windows;
using System.Windows.Input;

namespace TDC.Lib.Core.WPF.Behaviours
{
    public class MousePositionBehaviour : Microsoft.Xaml.Behaviors.Behavior<UIElement>
    {
        public static readonly DependencyProperty MouseLeftDownProperty = DependencyProperty.Register("MouseLeftDown", typeof(bool), typeof(MousePositionBehaviour), new PropertyMetadata(default(bool)));

        public bool MouseLeftDown
        {
            get { return (bool)GetValue(MouseLeftDownProperty); }
            set { SetValue(MouseLeftDownProperty, value); }
        }

        public static readonly DependencyProperty MouseLeftUpProperty = DependencyProperty.Register("MouseLeftUp", typeof(bool), typeof(MousePositionBehaviour), new PropertyMetadata(default(bool)));

        public bool MouseLeftUp
        {
            get { return (bool)GetValue(MouseLeftUpProperty); }
            set { SetValue(MouseLeftUpProperty, value); }
        }

        public static readonly DependencyProperty MouseYProperty = DependencyProperty.Register("MouseY", typeof(double), typeof(MousePositionBehaviour), new PropertyMetadata(default(double)));

        public double MouseY
        {
            get { return (double)GetValue(MouseYProperty); }
            set { SetValue(MouseYProperty, value); }
        }

        public static readonly DependencyProperty MouseXProperty = DependencyProperty.Register("MouseX", typeof(double), typeof(MousePositionBehaviour), new PropertyMetadata(default(double)));

        public double MouseX
        {
            get { return (double)GetValue(MouseXProperty); }
            set { SetValue(MouseXProperty, value); }
        }

        public static readonly DependencyProperty SelectionStartYProperty = DependencyProperty.Register("SelectionStartY", typeof(double), typeof(MousePositionBehaviour), new PropertyMetadata(default(double)));

        public double SelectionStartY
        {
            get { return (double)GetValue(SelectionStartYProperty); }
            set { SetValue(SelectionStartYProperty, value); }
        }

        public static readonly DependencyProperty SelectionStartXProperty = DependencyProperty.Register("SelectionStartX", typeof(double), typeof(MousePositionBehaviour), new PropertyMetadata(default(double)));

        public double SelectionStartX
        {
            get { return (double)GetValue(SelectionStartXProperty); }
            set { SetValue(SelectionStartXProperty, value); }
        }


        public static readonly DependencyProperty SelectionEndYProperty = DependencyProperty.Register("SelectionEndY", typeof(double), typeof(MousePositionBehaviour), new PropertyMetadata(default(double)));

        public double SelectionEndY
        {
            get { return (double)GetValue(SelectionEndYProperty); }
            set { SetValue(SelectionEndYProperty, value); }
        }

        public static readonly DependencyProperty SelectionEndXProperty = DependencyProperty.Register("SelectionEndX", typeof(double), typeof(MousePositionBehaviour), new PropertyMetadata(default(double)));

        public double SelectionEndX
        {
            get { return (double)GetValue(SelectionEndXProperty); }
            set { SetValue(SelectionEndXProperty, value); }
        }

        protected override void OnAttached()
        {
            AssociatedObject.MouseMove += AssociatedObjectOnMouseMove;
            AssociatedObject.MouseLeftButtonDown += AssociatedObjectMouseLeftButtonDown;
            AssociatedObject.MouseLeftButtonUp += AssociatedObjectOnMouseLeftButtonUp;
            AssociatedObject.MouseLeave += AssociatedObjectOnMouseLeave;
            AssociatedObject.MouseEnter += AssociatedObjectOnMouseEnter;
        }

        private void AssociatedObjectOnMouseEnter(object sender, MouseEventArgs args)
        {
            MouseLeftDown = false;
            MouseLeftUp = false;
        }

        private void AssociatedObjectOnMouseLeave(object sender, MouseEventArgs args)
        {
            MouseLeftDown = false;
            MouseLeftUp = false;
        }

        private void AssociatedObjectMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            MouseLeftDown = true;
            MouseLeftUp = false;
            var pos = e.GetPosition(AssociatedObject);
            SelectionStartX = pos.X;
            SelectionStartY = pos.Y;
        }

        private void AssociatedObjectOnMouseLeftButtonUp(object sender, MouseButtonEventArgs mouseButtonEventArgs)
        {
            MouseLeftDown = false;
            MouseLeftUp = true;
        }

        private void AssociatedObjectOnMouseMove(object sender, MouseEventArgs mouseEventArgs)
        {
            var pos = mouseEventArgs.GetPosition(AssociatedObject);
            MouseX = pos.X;
            MouseY = pos.Y;

            if (MouseLeftDown)
            {
                SelectionEndX = pos.X;
                SelectionEndY = pos.Y;
            }
        }

        protected override void OnDetaching()
        {
            AssociatedObject.MouseMove -= AssociatedObjectOnMouseMove;
            AssociatedObject.MouseLeftButtonDown -= AssociatedObjectMouseLeftButtonDown;
            AssociatedObject.MouseLeftButtonUp -= AssociatedObjectOnMouseLeftButtonUp;
            AssociatedObject.MouseLeave -= AssociatedObjectOnMouseLeave;
            AssociatedObject.MouseEnter -= AssociatedObjectOnMouseEnter;
        }
    }
}