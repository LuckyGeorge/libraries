﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace TDC.Lib.Core.WPF.Behaviours
{
    public class ListBoxRightClickSelectorBehaviour : System.Windows.Interactivity.Behavior<ListBox>
    {
        public static readonly DependencyProperty SelectedItemByRightClickProperty = DependencyProperty.Register("SelectedItemByRightClick", typeof(object), typeof(ListBoxRightClickSelectorBehaviour), new FrameworkPropertyMetadata(null,
            FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public object SelectedItemByRightClick
        {
            get { return GetValue(SelectedItemByRightClickProperty); }
            set { SetValue(SelectedItemByRightClickProperty, value); }
        }

        protected override void OnAttached()
        {
            AssociatedObject.PreviewMouseRightButtonDown += AssociatedObjectMouseRightButtonDown;
        }

        private void AssociatedObjectMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;

            if (e.OriginalSource is DependencyObject dpo)
            {
                if (sender is ListBox lb)
                {
                    if (ItemsControl.ContainerFromElement(lb, dpo) is ListBoxItem item &&
                        item.DataContext != SelectedItemByRightClick)
                    {
                        SelectedItemByRightClick = item.DataContext;
                        return;
                    }
                }
            }
            SelectedItemByRightClick = null;
        }

        protected override void OnDetaching()
        {
            AssociatedObject.PreviewMouseRightButtonDown -= AssociatedObjectMouseRightButtonDown;
        }
    }
}