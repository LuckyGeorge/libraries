﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Timers;
using System.Windows;
using System.Windows.Media;
using Timer = System.Timers.Timer;

namespace TDC.Lib.Core.WPF.Controls.TurtlePath
{
    /// <summary>
    /// Source: http://lexa.tatalata.com/l-systems-and-turtle-graphics-in-wpf/
    /// </summary>
    public class TurtlePathDisplay : FrameworkElement
    {
        public static readonly DependencyProperty PathProperty =
            DependencyProperty.Register(
                "Path",
                typeof(string),
                typeof(TurtlePathDisplay),
                new UIPropertyMetadata(string.Empty, OnDataChanged));

        public static readonly DependencyProperty TurtleParametersProperty =
            DependencyProperty.Register(
                "TurtleParameters",
                typeof(TurtleParameters),
                typeof(TurtlePathDisplay),
                new UIPropertyMetadata(new TurtleParameters(), OnDataChanged));

        private readonly VisualCollection children;
        private readonly Timer timer = new Timer(250);

        public TurtlePathDisplay()
        {
            children = new VisualCollection(this);
            var visual = new DrawingVisual();
            children.Add(visual);

            timer.Elapsed += OnTimerElapsed;
        }

        public string Path
        {
            get => (string) GetValue(PathProperty);
            set => SetValue(PathProperty, value);
        }

        public TurtleParameters TurtleParameters
        {
            get => (TurtleParameters) GetValue(TurtleParametersProperty);
            set => SetValue(TurtleParametersProperty, value);
        }

        protected override int VisualChildrenCount => children.Count;

        protected override Visual GetVisualChild(int index)
        {
            if (index < 0 || index >= children.Count)
                throw new ArgumentOutOfRangeException();

            return children[index];
        }

        private void InitTurtleParameters(ITurtle turtle, TurtleParameters parameters)
        {
            UpdateTurtleParameters(turtle, parameters);
            turtle.Angle = parameters.StartAngle;
        }

        private void MoveTurtle(ITurtle turtle)
        {
            var parameters = new TurtleParameters(TurtleParameters);

            InitTurtleParameters(turtle, parameters);

            var functions = new Dictionary<char, Action<ITurtle>>();
            functions['F'] = t => t.Forward(true);
            functions['f'] = t => t.Forward(false);
            functions['-'] = t => t.TurnLeft();
            functions['+'] = t => t.TurnRight();
            functions['['] = t => t.Push();
            functions[']'] = t => t.Pop();

            foreach (var c in Path)
            {
                Action<ITurtle> func;
                if (functions.TryGetValue(c, out func))
                {
                    func(turtle);

                    if (parameters.StepHandler != null)
                    {
                        parameters.StepHandler.Invoke(parameters);

                        UpdateTurtleParameters(turtle, parameters);
                    }
                }
            }
        }

        private static void OnDataChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            // Data and path usually change together, so to prevent double update of
            // visual representation, update will be done on timer.
            var timer = (obj as TurtlePathDisplay).timer;
            if (!timer.Enabled)
                timer.Start();
        }

        private void OnTimerElapsed(object sender, ElapsedEventArgs e)
        {
            (sender as Timer).Stop();
            Dispatcher.BeginInvoke(new ThreadStart(delegate { UpdatePathGraphics(); }));
        }

        private void UpdatePathGraphics()
        {
            if (TurtleParameters == null) return;

            // Move measurement turtle along the way, let it detect the Bounds of the path.
            var measureTurtle = new MeasurementTurtle();
            MoveTurtle(measureTurtle);

            // Update own size and render transformation.
            Height = measureTurtle.Bounds.Height;
            Width = measureTurtle.Bounds.Width;

            RenderTransform = new TransformGroup
            {
                Children = new TransformCollection
                {
                    new TranslateTransform(-measureTurtle.Bounds.X, -measureTurtle.Bounds.Y),
                    new ScaleTransform(1, -1, 0, Height / 2)
                }
            };

            // Update geometry.
            using (var dc = (children[0] as DrawingVisual).RenderOpen())
            {
                if (TurtleParameters.StartThickness == TurtleParameters.EndThickness)
                    MoveTurtle(new SimpleDrawingTurtle(
                        dc,
                        TurtleParameters.StartThickness,
                        TurtleParameters.UseRandomColors));
                else
                    using (var turtle = new DrawingTurtle(
                        dc,
                        measureTurtle.MaxLength,
                        TurtleParameters.StartThickness,
                        TurtleParameters.EndThickness,
                        TurtleParameters.UseRandomColors))
                    {
                        MoveTurtle(turtle);
                    }
            }

            InvalidateVisual();
        }

        private void UpdateTurtleParameters(ITurtle turtle, TurtleParameters parameters)
        {
            turtle.Distance = parameters.Distance;
            turtle.TurnAngle = parameters.TurnAngle;
        }
    }
}