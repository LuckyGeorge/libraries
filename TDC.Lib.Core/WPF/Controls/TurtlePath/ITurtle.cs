﻿using System.Windows;

namespace TDC.Lib.Core.WPF.Controls.TurtlePath
{
    /// <summary>
    /// Source: http://lexa.tatalata.com/l-systems-and-turtle-graphics-in-wpf/
    /// </summary>
    public interface ITurtle
    {
        double Angle { get; set; }
        double Distance { get; set; }

        Point Position { get; set; }
        double TurnAngle { get; set; }
        void Forward(bool doTrace);
        void Pop();

        void Push();
        void TurnLeft();
        void TurnRight();
    }
}