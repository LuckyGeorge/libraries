﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media;

namespace TDC.Lib.Core.WPF.Controls.TurtlePath
{
    /// <summary>
    /// Source: http://lexa.tatalata.com/l-systems-and-turtle-graphics-in-wpf/
    /// </summary>
    public class DrawingTurtle : MeasurementTurtle, IDisposable
    {
        private readonly DrawingContext context;
        private readonly double endThickness;
        private readonly double maxLength;
        private readonly Stack<List<Pair<Point, bool>>> paths = new Stack<List<Pair<Point, bool>>>();
        private readonly double startThickness;
        private readonly bool useRandomColors;

        public DrawingTurtle(DrawingContext context, double maxLength, double startWidth, double endWidth,
            bool useRandomColors)
        {
            this.context = context;
            startThickness = startWidth;
            endThickness = endWidth;
            this.maxLength = maxLength;
            this.useRandomColors = useRandomColors;

            paths.Push(new List<Pair<Point, bool>>());
        }

        private Point StartPoint
        {
            get
            {
                var paths = this.paths.ToArray();
                for (var i = 0; i < paths.Length; ++i)
                    if (paths[i].Count > 0)
                        return paths[i].Last().First;

                return new Point(0, 0);
            }
        }

        public void Dispose()
        {
            DrawPath(paths.Pop());
        }

        private void DrawPath(List<Pair<Point, bool>> list)
        {
            var pathLength = PathLength(list);
            var c = pathLength / maxLength;
            double length = 0;
            var previousPoint = StartPoint;
            var brush = useRandomColors ? BrushRandom.Next() : Brushes.Black;
            foreach (var t in list)
            {
                length += (t.First - previousPoint).Length;

                if (t.Second)
                {
                    double lineThickness = 0;
                    if (startThickness >= endThickness)
                        lineThickness = endThickness + (startThickness - endThickness) * (pathLength - length) /
                                        pathLength * c;
                    else
                        lineThickness = startThickness + (endThickness - startThickness) * (length * c) / pathLength;

                    context.DrawLine(new Pen(brush, lineThickness), previousPoint, t.First);
                }

                previousPoint = t.First;
            }
        }

        public override void Forward(bool doTrace)
        {
            base.Forward(doTrace);

            paths.Peek().Add(new Pair<Point, bool>(Position, doTrace));
        }

        private double PathLength(List<Pair<Point, bool>> list)
        {
            double length = 0;
            var previousPoint = StartPoint;
            foreach (var t in list)
            {
                length += (t.First - previousPoint).Length;
                previousPoint = t.First;
            }

            return length;
        }

        public override void Pop()
        {
            base.Pop();

            DrawPath(paths.Pop());
        }

        public override void Push()
        {
            base.Push();

            paths.Push(new List<Pair<Point, bool>>());
        }
    }
}