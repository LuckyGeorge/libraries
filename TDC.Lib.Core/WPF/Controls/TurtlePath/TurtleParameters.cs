﻿using System;

namespace TDC.Lib.Core.WPF.Controls.TurtlePath
{
    /// <summary>
    /// Source: http://lexa.tatalata.com/l-systems-and-turtle-graphics-in-wpf/
    /// </summary>
    public class TurtleParameters
    {
        public TurtleParameters()
        {
            Distance = 10;
            TurnAngle = 90;
            StartThickness = 1;
            EndThickness = 1;
        }

        public TurtleParameters(TurtleParameters parameters)
        {
            Distance = parameters.Distance;
            TurnAngle = parameters.TurnAngle;
            StartAngle = parameters.StartAngle;
            StartThickness = parameters.StartThickness;
            EndThickness = parameters.EndThickness;
            UseRandomColors = parameters.UseRandomColors;
            StepHandler = parameters.StepHandler;
        }

        // Value to move forward.
        public double Distance { get; set; }

        // Thickness of the last element.
        public double EndThickness { get; set; }

        // Start turtle angle (grads).
        public double StartAngle { get; set; }

        // Thickness of the first element.
        public double StartThickness { get; set; }

        // Handler, which will be called after every turtle action.
        public Action<TurtleParameters> StepHandler { get; set; }

        // Turn angle (grads).
        public double TurnAngle { get; set; }

        // If true, random colors will be used to draw lines.
        public bool UseRandomColors { get; set; }
    }
}