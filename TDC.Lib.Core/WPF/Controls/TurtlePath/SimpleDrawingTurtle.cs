﻿using System.Windows.Media;

namespace TDC.Lib.Core.WPF.Controls.TurtlePath
{
    /// <summary>
    /// Source: http://lexa.tatalata.com/l-systems-and-turtle-graphics-in-wpf/
    /// </summary>
    public class SimpleDrawingTurtle : MeasurementTurtle
    {
        private readonly DrawingContext context;
        private readonly double lineThickness;
        private readonly bool useRandomColors;

        public SimpleDrawingTurtle(DrawingContext context, double lineThickness, bool useRandomColors)
        {
            this.context = context;
            this.lineThickness = lineThickness;
            this.useRandomColors = useRandomColors;
        }

        public override void Forward(bool doTrace)
        {
            var start = Position;

            base.Forward(doTrace);

            var end = Position;

            if (doTrace)
                context.DrawLine(
                    new Pen(useRandomColors ? BrushRandom.Next() : Brushes.Black, lineThickness),
                    start,
                    end);
        }
    }
}