﻿using System;
using System.Windows.Media;

namespace TDC.Lib.Core.WPF.Controls.TurtlePath
{
    /// <summary>
    /// Source: http://lexa.tatalata.com/l-systems-and-turtle-graphics-in-wpf/
    /// </summary>
    internal class BrushRandom
    {
        private static readonly Random random = new Random();

        private static readonly Brush[] brushes =
        {
            Brushes.Black,
            Brushes.Red,
            Brushes.Green,
            Brushes.Blue,
            Brushes.Magenta,
            Brushes.DarkGoldenrod,
            Brushes.DarkGray,
            Brushes.DarkOrange,
            Brushes.Violet,
            Brushes.YellowGreen,
            Brushes.Tomato,
            Brushes.Olive
        };

        public static Brush Next()
        {
            return brushes[random.Next(brushes.Length)];
        }
    }
}