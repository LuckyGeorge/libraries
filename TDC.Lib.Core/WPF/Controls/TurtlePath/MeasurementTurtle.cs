﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace TDC.Lib.Core.WPF.Controls.TurtlePath
{
    /// <summary>
    /// Source: http://lexa.tatalata.com/l-systems-and-turtle-graphics-in-wpf/
    /// </summary>
    public class MeasurementTurtle : ITurtle
    {
        private readonly Stack<Triad<Point, double, double>> branches = new Stack<Triad<Point, double, double>>();

        public MeasurementTurtle()
        {
            branches.Push(new Triad<Point, double, double>(new Point(0, 0), 0, 0));
        }

        public Rect Bounds { get; protected set; }

        public double Length
        {
            get => branches.Peek().Third;

            protected set
            {
                branches.Peek().Third = value;
                MaxLength = Math.Max(MaxLength, value);
            }
        }

        public double MaxLength { get; protected set; }

        public double Distance { get; set; }

        public double TurnAngle { get; set; }

        public Point Position
        {
            get => branches.Peek().First;

            set
            {
                branches.Peek().First = value;
                var b = Bounds;
                b.Union(value);
                Bounds = b;
            }
        }

        public double Angle
        {
            get => branches.Peek().Second;
            set => branches.Peek().Second = value;
        }

        public virtual void Push()
        {
            branches.Push(new Triad<Point, double, double>(branches.Peek()));
        }

        public virtual void Pop()
        {
            branches.Pop();
        }

        public virtual void Forward(bool doTrace)
        {
            var pos = Position;

            pos.Offset(
                Distance * Math.Cos(Angle * Math.PI / 180.0),
                Distance * Math.Sin(Angle * Math.PI / 180.0));

            Position = pos;

            Length += Distance;
        }

        public virtual void TurnLeft()
        {
            Angle -= TurnAngle;
        }

        public virtual void TurnRight()
        {
            Angle += TurnAngle;
        }
    }
}