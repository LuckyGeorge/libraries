﻿namespace TDC.Lib.Core.WPF.Controls.TurtlePath
{
    /// <summary>
    /// Source: http://lexa.tatalata.com/l-systems-and-turtle-graphics-in-wpf/
    /// </summary>
    public class Triad<T, U, V>
    {
        public Triad()
        {
        }

        public Triad(T first, U second, V third)
        {
            First = first;
            Second = second;
            Third = third;
        }

        public Triad(Triad<T, U, V> t)
        {
            First = t.First;
            Second = t.Second;
            Third = t.Third;
        }

        public T First { get; set; }
        public U Second { get; set; }
        public V Third { get; set; }
    }
}